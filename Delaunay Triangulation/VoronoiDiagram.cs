﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DelaunayTriangulation {

	/// <summary>
	/// Constructed from the Delaunay triangulation.
	/// For each cell, stores its vertex and polygon enclosing the cell.
	/// </summary>
	public class VoronoiDiagram {



		//temporary storage associating cells defined by vertices and all their corresponding edges
		//later, they are used to produce enclosing polygons for cells
		private Dictionary<Point2D, HashSet<TriangleSet.Edge>> cellEdges;

		/// <summary>
		/// Polygon enclosing each Voronoi cell defined by its center - triangle vertex.
		/// If cell is on the border, polygon stretched to infinity is cut
		/// in distance sufficient to fill semi-closed cell completely when painting.
		/// </summary>
		public Dictionary<Point2D, List<Point2D>> CellPolygons { get; private set; }

		/// <summary>
		/// Set of all edges of Voronoi diagram.
		/// Useful when painting only cell borders.
		/// </summary>
		public HashSet<TriangleSet.Edge> Edges { get; set; }



		/// <summary>
		/// Constructs Voronoi diagram from Dealaunay triangulation.
		/// Works in O(triangles) = O(vertices) with some ugly constants.
		/// </summary>
		/// <param name="dt"></param>
		public VoronoiDiagram(DelaunayTriangulation dt) {
			cellEdges = new Dictionary<Point2D, HashSet<TriangleSet.Edge>>();
			CellPolygons = new Dictionary<Point2D, List<Point2D>>();
			Edges = new HashSet<TriangleSet.Edge>();
			dt.TriangleSet.Triangles.ForEach(tr => addEdges(dt, tr));
			cellEdges.ForEach(rec => CellPolygons[rec.Key] = makePolygonFromEdges(rec.Value));
		}




		//adds edges found from Delaunay triangle to the cells corresponding to triangle vertices.
		//notice that this adds only some of the edges for the cell, and cell is completely
		//bounded only after iterating all triangles.
		//works in constant time.
		private void addEdges(DelaunayTriangulation dt, TriangleSet.Triangle tr) {

			Point2D center = GeomUtils.TriangleCircumcenter(tr.Vertices[0], tr.Vertices[1], tr.Vertices[2]);
			bool isCenterInside = GeomUtils.PointInTriangle(center, tr.Vertices[0], tr.Vertices[1], tr.Vertices[2]);
			//minimum distance of circumcenter to edge midpoints
			float minDistMidToCenter = float.MaxValue;
			tr.ForEachEdge((i, v1, v2) => {
				minDistMidToCenter = Math.Min(
					minDistMidToCenter,
					GeomUtils.DistEuclid(center, GeomUtils.LineSegmentMidpoint(v1, v2)));
			});

			tr.ForEachEdge((i, v1, v2) => {

				TriangleSet.Triangle neig = tr.Neighbours[i];

				//edges for v1 and v2
				HashSet<TriangleSet.Edge> edges1;
				//add to existing edge set if it already exists, or create new
				if (!cellEdges.TryGetValue(v1, out edges1)) {
					edges1 = new HashSet<TriangleSet.Edge>();
					cellEdges[v1] = edges1;
				}
				HashSet<TriangleSet.Edge> edges2;
				if (!cellEdges.TryGetValue(v2, out edges2)) {
					edges2 = new HashSet<TriangleSet.Edge>();
					cellEdges[v2] = edges2;
				}

				//one edge of voronoi cell boundary, colinear with current triangle edge bisector
				TriangleSet.Edge voronoiEdge;

				//goes to infinity (a.k.a number big enough)
				if (neig == null) {
					
					//passes through triangle edge midpoint
					Point2D mid = GeomUtils.LineSegmentMidpoint(v1, v2);
					Point2D center2 = center;

					//if triangle has right angle, center will be located on edge
					//in this case, we must find new voronoi edge by other method
					bool oppositeEdgeOfRightAngle = GeomUtils.PointOnLineSegment(center, v1, v2);
					if (oppositeEdgeOfRightAngle) {
						//find points on both sides of bisector of edge
						Point2D tmp1 = new Point2D(mid.x + (v1.y - mid.y), mid.y - (v1.x - mid.x));
						Point2D tmp2 = new Point2D(mid.x - (v1.y - mid.y), mid.y + (v1.x - mid.x));
						//find the one closer to the opposite edge vertex, which has right angle. call it X.
						//then we will have voronoi edge starting in circumcenter and going
						//in direction from X to circumcenter.
						Point2D opposite = tr.OppositeVertex(new TriangleSet.Edge(v1, v2));
						if (GeomUtils.DistEuclidSq(opposite, tmp1) < GeomUtils.DistEuclidSq(opposite, tmp2)) {
							center2 = tmp1;
						} else {
							center2 = tmp2;
						}
					}
					
					//we will make voronoi edge end in some sufficient distance (-2000 or 2000)
					float scale = 2000 / Math.Max(1, Math.Min(Math.Abs(mid.x - center2.x), Math.Abs(mid.y - center2.y)));
					
					//find direction
					//1. if center is inside triangle, all voronoi edges start in circumcenter, pass by midpoint and go further
					//2. if circumcenter is outside, for two edges more distant from the center the same applies.
					//   however, for triangle edge closest to the center, voronoi edge starts in center and
					//   goes in opposite direction (still perpendicular to triangle edge).
					//3. special case for right triangle, see previous part of code
					if (!oppositeEdgeOfRightAngle && !isCenterInside && GeomUtils.DistEuclid(center, mid) == minDistMidToCenter) {
						scale *= -1;
					}
					
					voronoiEdge = new TriangleSet.Edge(
						center,
						new Point2D(center.x + (mid.x - center2.x) * scale, center.y + (mid.y - center2.y) * scale));
				
				//finite boundary edge - line segment 
				} else {
					//connect with neighbour's circumcenter
					Point2D centerN = GeomUtils.TriangleCircumcenter(neig.Vertices[0], neig.Vertices[1], neig.Vertices[2]);
					voronoiEdge = new TriangleSet.Edge(center, centerN);
				}

				Edges.Add(voronoiEdge);
				edges1.Add(voronoiEdge);
				edges2.Add(voronoiEdge);
			});

		}




		//fills CellPolygons
		//lists of edges for vertices are transformed to sorted point list so that they can
		//represent polygons enclosing voronoi cell
		private List<Point2D> makePolygonFromEdges(HashSet<TriangleSet.Edge> edges) {

			//all points of polygon, we want to order them
			var pointSet = new HashSet<Point2D>();
			edges.ForEach(e => {
				pointSet.Add(e.a);
				pointSet.Add(e.b);
			});
			//start with arbitrary and spred into both directions
			var polygon = new List<Point2D>() { pointSet.First() };
			pointSet.Remove(pointSet.First());

			//append points to the polygon from both sides until all points are added
			while (pointSet.HasContent()) {
				//append
				Point2D? point = getNextPoint(edges, polygon.Last(), pointSet);
				if (point == null) {
					//cannot append, add to the beggining
					point = getNextPoint(edges, polygon.First(), pointSet);
					if (point == null) {
						//this really shouldn't happen
						throw new Exception("Voronoi cell boundaries inconsistency!");
					}
					polygon.Insert(0, point.Value);
				} else {
					polygon.Add(point.Value);
				}
				pointSet.Remove(point.Value);
			}

			return polygon;
		}



		//finds neighbouring point to the given point, effectively finding the next point in the polygon
		//new point must be present in a given point set
		private Point2D? getNextPoint(HashSet<TriangleSet.Edge> edges, Point2D point, HashSet<Point2D> fromSet) {
			foreach (TriangleSet.Edge edge in edges) {
				if (edge.a.Equals(point) && fromSet.Contains(edge.b)) {
					return edge.b;
				}
				if (edge.b.Equals(point) && fromSet.Contains(edge.a)) {
					return edge.a;
				}
			}
			return null;
		}

	}
}
