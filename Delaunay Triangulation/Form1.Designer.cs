﻿namespace DelaunayTriangulation {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.panel1 = new System.Windows.Forms.Panel();
			this.checkBoxShowCenters = new System.Windows.Forms.CheckBox();
			this.checkBoxCircles = new System.Windows.Forms.CheckBox();
			this.checkBoxDelaunay = new System.Windows.Forms.CheckBox();
			this.checkBoxVoronoi = new System.Windows.Forms.CheckBox();
			this.labelShow = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.checkBoxGrid = new System.Windows.Forms.CheckBox();
			this.labelNewPoints = new System.Windows.Forms.Label();
			this.numericUpDownAddPoints = new System.Windows.Forms.NumericUpDown();
			this.buttonAddPoints = new System.Windows.Forms.Button();
			this.buttonClear = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownAddPoints)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.Location = new System.Drawing.Point(0, -1);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(579, 556);
			this.panel1.TabIndex = 0;
			this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
			this.panel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseClick);
			// 
			// checkBoxShowCenters
			// 
			this.checkBoxShowCenters.AutoSize = true;
			this.checkBoxShowCenters.Checked = true;
			this.checkBoxShowCenters.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxShowCenters.Location = new System.Drawing.Point(12, 65);
			this.checkBoxShowCenters.Name = "checkBoxShowCenters";
			this.checkBoxShowCenters.Size = new System.Drawing.Size(93, 17);
			this.checkBoxShowCenters.TabIndex = 1;
			this.checkBoxShowCenters.Text = "Circumcenters";
			this.checkBoxShowCenters.UseVisualStyleBackColor = true;
			this.checkBoxShowCenters.CheckedChanged += new System.EventHandler(this.checkBoxShowCenters_CheckedChanged);
			// 
			// checkBoxCircles
			// 
			this.checkBoxCircles.AutoSize = true;
			this.checkBoxCircles.Checked = true;
			this.checkBoxCircles.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxCircles.Location = new System.Drawing.Point(12, 88);
			this.checkBoxCircles.Name = "checkBoxCircles";
			this.checkBoxCircles.Size = new System.Drawing.Size(88, 17);
			this.checkBoxCircles.TabIndex = 2;
			this.checkBoxCircles.Text = "Circumcircles";
			this.checkBoxCircles.UseVisualStyleBackColor = true;
			this.checkBoxCircles.CheckedChanged += new System.EventHandler(this.checkBoxCircles_CheckedChanged);
			// 
			// checkBoxDelaunay
			// 
			this.checkBoxDelaunay.AutoSize = true;
			this.checkBoxDelaunay.Checked = true;
			this.checkBoxDelaunay.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxDelaunay.Location = new System.Drawing.Point(12, 42);
			this.checkBoxDelaunay.Name = "checkBoxDelaunay";
			this.checkBoxDelaunay.Size = new System.Drawing.Size(71, 17);
			this.checkBoxDelaunay.TabIndex = 3;
			this.checkBoxDelaunay.Text = "Delaunay";
			this.checkBoxDelaunay.UseVisualStyleBackColor = true;
			this.checkBoxDelaunay.CheckedChanged += new System.EventHandler(this.checkBoxDelaunay_CheckedChanged);
			// 
			// checkBoxVoronoi
			// 
			this.checkBoxVoronoi.AutoSize = true;
			this.checkBoxVoronoi.Location = new System.Drawing.Point(12, 19);
			this.checkBoxVoronoi.Name = "checkBoxVoronoi";
			this.checkBoxVoronoi.Size = new System.Drawing.Size(62, 17);
			this.checkBoxVoronoi.TabIndex = 4;
			this.checkBoxVoronoi.Text = "Voronoi";
			this.checkBoxVoronoi.UseVisualStyleBackColor = true;
			this.checkBoxVoronoi.CheckedChanged += new System.EventHandler(this.checkBoxVoronoi_CheckedChanged);
			// 
			// labelShow
			// 
			this.labelShow.AutoSize = true;
			this.labelShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.labelShow.Location = new System.Drawing.Point(12, 0);
			this.labelShow.Name = "labelShow";
			this.labelShow.Size = new System.Drawing.Size(38, 13);
			this.labelShow.TabIndex = 5;
			this.labelShow.Text = "Show";
			// 
			// panel2
			// 
			this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel2.Controls.Add(this.label1);
			this.panel2.Controls.Add(this.checkBoxGrid);
			this.panel2.Controls.Add(this.labelNewPoints);
			this.panel2.Controls.Add(this.numericUpDownAddPoints);
			this.panel2.Controls.Add(this.buttonAddPoints);
			this.panel2.Controls.Add(this.buttonClear);
			this.panel2.Controls.Add(this.labelShow);
			this.panel2.Controls.Add(this.checkBoxShowCenters);
			this.panel2.Controls.Add(this.checkBoxVoronoi);
			this.panel2.Controls.Add(this.checkBoxCircles);
			this.panel2.Controls.Add(this.checkBoxDelaunay);
			this.panel2.Location = new System.Drawing.Point(585, 12);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(148, 530);
			this.panel2.TabIndex = 1;
			// 
			// checkBoxGrid
			// 
			this.checkBoxGrid.AutoSize = true;
			this.checkBoxGrid.Location = new System.Drawing.Point(12, 148);
			this.checkBoxGrid.Name = "checkBoxGrid";
			this.checkBoxGrid.Size = new System.Drawing.Size(83, 17);
			this.checkBoxGrid.TabIndex = 10;
			this.checkBoxGrid.Text = "Snap to grid";
			this.checkBoxGrid.UseVisualStyleBackColor = true;
			this.checkBoxGrid.CheckedChanged += new System.EventHandler(this.checkBoxGrid_CheckedChanged);
			// 
			// labelNewPoints
			// 
			this.labelNewPoints.AutoSize = true;
			this.labelNewPoints.Location = new System.Drawing.Point(9, 206);
			this.labelNewPoints.Name = "labelNewPoints";
			this.labelNewPoints.Size = new System.Drawing.Size(79, 13);
			this.labelNewPoints.TabIndex = 9;
			this.labelNewPoints.Text = "Random Points";
			// 
			// numericUpDownAddPoints
			// 
			this.numericUpDownAddPoints.Location = new System.Drawing.Point(86, 222);
			this.numericUpDownAddPoints.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numericUpDownAddPoints.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericUpDownAddPoints.Name = "numericUpDownAddPoints";
			this.numericUpDownAddPoints.Size = new System.Drawing.Size(56, 20);
			this.numericUpDownAddPoints.TabIndex = 8;
			this.numericUpDownAddPoints.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// buttonAddPoints
			// 
			this.buttonAddPoints.Location = new System.Drawing.Point(5, 222);
			this.buttonAddPoints.Name = "buttonAddPoints";
			this.buttonAddPoints.Size = new System.Drawing.Size(75, 23);
			this.buttonAddPoints.TabIndex = 7;
			this.buttonAddPoints.Text = "Add";
			this.buttonAddPoints.UseVisualStyleBackColor = true;
			this.buttonAddPoints.Click += new System.EventHandler(this.buttonAddPoints_Click);
			// 
			// buttonClear
			// 
			this.buttonClear.Location = new System.Drawing.Point(5, 284);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.Size = new System.Drawing.Size(75, 23);
			this.buttonClear.TabIndex = 6;
			this.buttonClear.Text = "Clear";
			this.buttonClear.UseVisualStyleBackColor = true;
			this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 465);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(104, 65);
			this.label1.TabIndex = 11;
			this.label1.Text = "LMB: add new point\r\nRMB: fill Voronoi cell\r\n\r\n© Matej Kopernický\r\n2014";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(745, 554);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.MinimumSize = new System.Drawing.Size(500, 400);
			this.Name = "Form1";
			this.Text = "Delaunay Triangulation + Voronoi Diagram";
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownAddPoints)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.CheckBox checkBoxShowCenters;
		private System.Windows.Forms.CheckBox checkBoxCircles;
		private System.Windows.Forms.CheckBox checkBoxDelaunay;
		private System.Windows.Forms.CheckBox checkBoxVoronoi;
		private System.Windows.Forms.Label labelShow;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Label labelNewPoints;
		private System.Windows.Forms.NumericUpDown numericUpDownAddPoints;
		private System.Windows.Forms.Button buttonAddPoints;
		private System.Windows.Forms.CheckBox checkBoxGrid;
		private System.Windows.Forms.Label label1;
	}
}

