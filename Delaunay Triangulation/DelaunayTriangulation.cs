﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DelaunayTriangulation {
	public class DelaunayTriangulation {



		public TriangleSet TriangleSet { get; private set; }
		
		/// <summary>
		/// <c>True</c> if there is no triangle yet.
		/// </summary>
		public bool Initializing { get; private set; }
		
		/// <summary>
		/// List of points not yet added to triangles when initializing.
		/// </summary>
		public List<Point2D> PointsInit { get; private set; }

		//for fixing
		private Queue<TriangleSet.Edge> queue = new Queue<TriangleSet.Edge>();



		public DelaunayTriangulation() {
			Clear();
		}



		public void Clear() {
			Initializing = true;
			PointsInit = new List<Point2D>();
			TriangleSet = new TriangleSet();
		}
		


		/// <summary>
		/// Adds new point to the triangulation, recalculating if necessary.
		/// Works in O(vertices^2) in worst case.
		/// </summary>
		public void AddPoint(Point2D point) {

			if (TriangleSet.Points.Contains(point)) {
				return;
			}
			TriangleSet.Points.Add(point);

			//waiting to create first triangle
			if (Initializing) {
				PointsInit.Add(point);
				//can create triangle(s) only if points are not aligned.
				//after adding first non-aligned point, we can connect it with all neighbouring pairs of aligned points.
				//trivially, in case of 3 points this creates first single triangle (usual situation).
				if (PointsInit.Count >= 3 && !GeomUtils.PointsAligned(PointsInit.ToArray())) {
					PointsInit.Remove(point);
					PointsInit = PointsInit.OrderBy(p => p.x).ThenBy(p => p.y).ToList();
					for (int i = 1; i < PointsInit.Count; ++i) {
						TriangleSet.AddTriangle(PointsInit[i - 1], PointsInit[i], point);
					}
					Initializing = false;
					PointsInit.Clear();
				}
			
			} else {

				queue.Clear();

				TriangleSet.Edge onEdge = null;
				//runs in O(vertices), as graph is planar and has at most 3*vertices-6 edges
				TriangleSet.EdgeTriangle.Keys.ForEach(e => {
					if (GeomUtils.PointOnLineSegment(point, e.a, e.b)) {
						onEdge = new TriangleSet.Edge(e.a, e.b);
					}
				});

				TriangleSet.Triangle containing = onEdge != null ? null : TriangleSet.GetContainingTriangle(point);
				
				if (onEdge != null) {
					//on edge of triangle
					//we will split adjacent triangles of edge into two
					var adjacentTriangles = TriangleSet.EdgeTriangle[onEdge];
					if (adjacentTriangles.Item1 != null) {
						splitTriangleOnEdge(adjacentTriangles.Item1, onEdge, point);
					}
					if (adjacentTriangles.Item2 != null) {
						splitTriangleOnEdge(adjacentTriangles.Item2, onEdge, point);
					}

				} else if (containing != null) {
					//inside of triangle
					addInner(point, containing);
				
				} else {	
					//outside of convex hull of current points
					addOuter(point);
				}
				
				fix(queue);
			}
		}



		private void addOuter(Point2D point) {
			//connect to all visible edges
			var edgeRecords = TriangleSet.GetVisibleEdges(point).ToList();
			foreach (var edgeRecord in edgeRecords) {
				TriangleSet.Triangle newTriangle = TriangleSet.AddTriangle(point, edgeRecord.Item2, edgeRecord.Item3);
				//common edge of new triangle with the connected one
				queue.Enqueue(newTriangle.CommonEdge(edgeRecord.Item1));
			}
		}



		private void addInner(Point2D point, TriangleSet.Triangle tr) {
			//three new triangles
			List<TriangleSet.Triangle> newTriangles = TriangleSet.AddVertexInsideTriangle(point, tr).ToList();
			//fix new triangles
			foreach (TriangleSet.Triangle newTriangle in newTriangles) {
				//check their outside neighbours (if they exist)
				foreach (TriangleSet.Triangle neighbour in
					newTriangle.Neighbours.Where(n => n != null && !newTriangles.Contains(n)).ToList()) {
					queue.Enqueue(newTriangle.CommonEdge(neighbour));
				}
			}
		}



		private void splitTriangleOnEdge(TriangleSet.Triangle tr, TriangleSet.Edge edge, Point2D point) {
			Point2D opposite = tr.OppositeVertex(edge);
			TriangleSet.RemoveTriangle(tr);
			var tr1 = TriangleSet.AddTriangle(edge.a, point, opposite);
			var tr2 = TriangleSet.AddTriangle(edge.b, point, opposite);
			tr1.ForEachEdge((_, v1, v2) => queue.Enqueue(new TriangleSet.Edge(v1, v2)));
			tr2.ForEachEdge((_, v1, v2) => queue.Enqueue(new TriangleSet.Edge(v1, v2)));
		}



		private void fix(Queue<TriangleSet.Edge> queue) {

			while (!queue.IsEmpty()) {
				
				TriangleSet.Edge edge = queue.Dequeue();
				//if there are some old edges that were already removed
				if (!TriangleSet.EdgeTriangle.ContainsKey(edge)) {
					return;
				}
				//triangles adjacent to the edge
				var triangles = TriangleSet.EdgeTriangle[edge];
				TriangleSet.Triangle f1 = triangles.Item1;
				TriangleSet.Triangle f2 = triangles.Item2;
				//nothing to fix
				if (f1 == null || f2 == null) {
					continue;
				}
				Point2D f1Distant = f1.OppositeVertex(f2);
				Point2D f2Distant = f2.OppositeVertex(f1);

				//needs flip?
				if (GeomUtils.PointInTriangleCircumcircle(f2Distant, f1.Vertices[0], f1.Vertices[1], f1.Vertices[2]) ||
					GeomUtils.PointInTriangleCircumcircle(f1Distant, f2.Vertices[0], f2.Vertices[1], f2.Vertices[2])) {
						
					//common vertices of two triangles
					List<Point2D> adjacent = f1.AdjacentVertices(f2).ToList();
					TriangleSet.RemoveTriangle(f1);
					TriangleSet.RemoveTriangle(f2);
					TriangleSet.Triangle newF1 = TriangleSet.AddTriangle(adjacent[0], f1Distant, f2Distant);
					TriangleSet.Triangle newF2 = TriangleSet.AddTriangle(adjacent[1], f1Distant, f2Distant);
					//check next neighbours
					foreach (TriangleSet.Triangle neighbour in newF1.Neighbours.Where(n => n != null && !n.In(newF2, f1, f2)).ToList()) {
						adjacent = newF1.AdjacentVertices(neighbour).ToList();
						queue.Enqueue(new TriangleSet.Edge(adjacent[0], adjacent[1]));
					}
					foreach (TriangleSet.Triangle neighbour in newF2.Neighbours.Where(n => n != null && !n.In(newF1, f1, f2)).ToList()) {
						adjacent = newF2.AdjacentVertices(neighbour).ToList();
						queue.Enqueue(new TriangleSet.Edge(adjacent[0], adjacent[1]));
					}
				}
			}

		}



	}
}
