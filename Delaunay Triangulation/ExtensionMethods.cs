﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DelaunayTriangulation {

	/// <summary>
	/// Bunch of useful extension methods I miss in .NET.
	/// Partially inspired by Jon Skeet's morelinq: http://code.google.com/p/morelinq/
	/// </summary>
	public static class ExtensionMethods {



		public static void Debug<T>(this IEnumerable<T> source, string label = null) {
			if (label.HasContent()) {
				Console.Write(label + ": ");
			}
			if (source == null) {
				Console.WriteLine("null>");
			} else {
				string str = source.Aggregate<T, string>("", (s, e) => s + toString(e) + ", ");
				Console.WriteLine("[" + str.Substring(0, Math.Max(0, str.Length - 2)) + "]");
			}
		}

		public static void Debug(this string str) {
			Console.WriteLine(toString(str));
		}

		public static void Debug(this object obj, string label = null) {
			if (label.HasContent()) {
				Console.Write(label + ": ");
			}
			Console.WriteLine(toString(obj));
		}

		private static string toString(object obj) {
			return obj == null ? "<null>" : obj.ToString();
		}


		
		//two different methods for IEnumerable and ICollection
		//because IEnumerable's Count method is not necessarily O(1),
		//while ICollection's property usually is.

		public static bool IsEmpty<T>(this IEnumerable<T> source) {
			if (source == null) {
				throw new NullReferenceException();
			}
			return source.Count() == 0;
		}

		public static bool IsEmpty<T>(this ICollection<T> source) {
			if (source == null) {
				throw new NullReferenceException();
			}
			return source.Count == 0;
		}

		public static bool HasContent<T>(this IEnumerable<T> source) {
			if (source == null) {
				throw new NullReferenceException();
			}
			return source.Count() > 0;
		}

		public static bool HasContent<T>(this ICollection<T> source) {
			if (source == null) {
				throw new NullReferenceException();
			}
			return source.Count > 0;
		}



		/// <summary>
		/// Returns whether given number is real (is not infinity or NaN).
		/// </summary>
		public static bool isNumber(this double d) {
			return !double.IsNaN(d) && !double.IsInfinity(d);
		}

		/// <summary>
		/// Returns whether given number is real (is not infinity or NaN).
		/// </summary>
		public static bool isNumber(this float f) {
			return !float.IsNaN(f) && !float.IsInfinity(f);
		}



		/// <summary>
		/// Checks whether an object is contained among the elements of a given list.
		/// </summary>
		public static bool In<T>(this T source, params T[] list) {
			if (null == source) {
				throw new NullReferenceException();
			}
			return list.Contains(source);
		}



		/// <summary>
		/// Apply action on each element of the source list.
		/// </summary>
		/// <exception cref="NullReferenceException">Thrown when source list or action is null.</exception>
		public static void ForEach<T>(this IEnumerable<T> source, Action<T> action) {
			if (source == null || action == null) {
				throw new NullReferenceException();
			}
			foreach (T element in source) {
				action(element);
			}
		}



		/// <summary>
		/// Apply action on each element of the source list.
		/// </summary>
		/// <exception cref="NullReferenceException">Thrown when source list or action is null.</exception>
		public static void For<T>(this IEnumerable<T> source, Action<int, T> action) {
			if (source == null || action == null) {
				throw new NullReferenceException();
			}
			int idx = 0;
			foreach (T element in source) {
				action(idx++, element);
			}
		}



		/// <summary>
		/// Throws <c>ArgumentNullException</c> when an object is null.
		/// Drawback of this way of exception throwing is that thrown exception will end up
		/// with this method on the top of the call stack.
		/// </summary>
		/// <param name="exceptionMessage">Optional text of the exception.</param>
		public static void ThrowIfNull(this object obj, string exceptionMessage = null) {
			if (obj == null) {
				if (exceptionMessage.IsNullOrEmpty()) {
					throw new ArgumentNullException();
				} else {
					throw new ArgumentNullException(exceptionMessage);
				}
			}
		}



		/// <summary>
		/// Throws <c>ArgumentNullException</c> when string is null or
		/// <c>ArgumentException</c> when empty.
		/// Drawback of this way of exception throwing is that thrown exception will end up
		/// with this method on the top of the call stack.
		/// </summary>
		/// <param name="exceptionMessage">Optional text of the exception.</param>
		public static void ThrowIfNullOrEmpty(this string str, string exceptionMessage = null) {
			if (str == null) {
				if (String.IsNullOrEmpty(exceptionMessage)) {
					throw new ArgumentNullException();
				} else {
					throw new ArgumentNullException(exceptionMessage);
				}
			}
			if (str == string.Empty) {
				if (String.IsNullOrEmpty(exceptionMessage)) {
					throw new ArgumentException();
				} else {
					throw new ArgumentException(exceptionMessage);
				}
			}
		}



		/// <summary>
		/// Returns all non null elements.
		/// </summary>
		public static IEnumerable<T> NonNullElements<T>(this IEnumerable<T> source) {
			if (source == null) {
				throw new NullReferenceException();
			}
			return source.Where(x => x != null);
		}



		/// <summary>
		/// Returns all non-null non-empty elements.
		/// </summary>
		public static IEnumerable<string> NonNullNonEmptyElements(this IEnumerable<string> source) {
			if (source == null) {
				throw new NullReferenceException();
			}
			return source.Where(x => x.HasContent());
		}



		/// <summary>
		/// Safely raises an event, checking for null and taking care of race conditions.
		/// </summary>
		public static void Raise<T>(this EventHandler<T> handler, object sender, T args) where T : EventArgs {
			if (handler != null) {
				handler(sender, args);
			}
		}



		/// <summary>
		/// Calls the <c>IsNullOrEmpty</c> method on a given string.
		/// I just didn't like the original static nature of the method.
		/// </summary>
		public static bool IsNullOrEmpty(this string str) {
			return String.IsNullOrEmpty(str);
		}



		/// <summary>
		/// Calls the <c>IsNullOrEmpty</c> method on a given string
		/// and returns its negated return value.
		/// I just didn't like the original static nature of the method and
		/// also wanted it to heave positive meaning.
		/// </summary>
		public static bool HasContent(this string str) {
			return !String.IsNullOrEmpty(str);
		}



		/// <summary>
		/// Check whether a comparable object is between two values.
		/// By default, the interval is considered
		/// </summary>
		/// <param name="left">To return true, an object must be higher (or equal if <c>excludeLeft</c> is <c>false</c>)
		/// than the value given by this parameter.</param>
		/// <param name="left">To return true, an object must be lower (or equal if <c>excludeRight</c> is <c>false</c>)
		/// than the value given by this parameter.</param>
		/// <param name="excludeLeft">If set to true, an object equal to the <c>left</c> value will return false.</param>
		/// <param name="excludeRight">If set to true, an object equal to the <c>right</c> value will return false.</param>
		public static bool Between<T>(this T value, T left, T right, bool excludeLeft = false, bool excludeRight = false)
		where T : IComparable<T> {
			if (value == null) {
				throw new NullReferenceException();
			}
			int leftComp = value.CompareTo(left);
			bool leftOk = excludeLeft ? leftComp > 0 : leftComp >= 0;
			int rightComp = value.CompareTo(right);
			bool rightOk = excludeRight ? rightComp < 0 : rightComp <= 0;
			return leftOk && rightOk;
		}


	}
}
