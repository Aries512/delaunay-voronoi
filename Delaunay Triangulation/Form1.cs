﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DelaunayTriangulation {
	public partial class Form1 : Form {
		
		
		
		public Form1() {
			InitializeComponent();
		}


		private const int GRID_SIZE = 20;

		private DelaunayTriangulation dt = new DelaunayTriangulation();
		private VoronoiDiagram vd;

		private Pen gridPen = new Pen(Color.LightGray, 1);
		private Pen edgeDelaunayPen = new Pen(Color.Black, 2);
		private Pen edgeVoronoiPen = new Pen(Color.Green, 2);
		private Pen centerPen = new Pen(Color.Blue, 2);
		private Pen circlePen = new Pen(Color.Red, 1);
		private Brush cellBrush = new SolidBrush(Color.FromArgb(235, 235, 235));

		//stores whether we have already painted edge or vertex
		//prevents repeated repainting
		private HashSet<TriangleSet.Edge> paintedEdges;
		private HashSet<Point2D> paintedVertices;

		//currently painted cell
		//resets after adding point
		private Point2D? paintedVoronoiCellVertex = null;

		private Random rand = new Random((int)DateTime.Now.Ticks);



		private void panel1_Paint(object sender, PaintEventArgs e) {

			Graphics g = panel1.CreateGraphics();
			g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
			//g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

			paintedEdges = new HashSet<TriangleSet.Edge>();
			paintedVertices = new HashSet<Point2D>();

			//draw only points
			if (dt.Initializing && checkBoxDelaunay.Checked) {
				dt.PointsInit.ForEach(p => drawVertex(g, centerPen, p.x, p.y));
			}
			
			//fill cells first so they don't cover edges and stuff
			if (checkBoxVoronoi.Checked && vd != null && paintedVoronoiCellVertex != null) {
				var polygon = vd.CellPolygons[paintedVoronoiCellVertex.Value];
				g.FillPolygon(cellBrush, polygon.Select(p => new PointF(p.x, p.y)).ToArray());
			}

			//draw grid
			if (checkBoxGrid.Checked) {
				for (int i = GRID_SIZE; i < panel1.Height; i += GRID_SIZE) {
					g.DrawLine(gridPen, new Point(0, i), new Point(panel1.Width, i));
				}
				for (int i = GRID_SIZE; i < panel1.Width; i += GRID_SIZE) {
					g.DrawLine(gridPen, new Point(i, 0), new Point(i, panel1.Height));
				}
			}

			foreach (TriangleSet.Triangle tr in dt.TriangleSet.Triangles) {

				//triangulation
				if (checkBoxDelaunay.Checked) {
					tr.ForEachEdge((_, v1, v2) => drawEdge(g, edgeDelaunayPen, v1, v2));
				}
				
				//points
				if (checkBoxDelaunay.Checked || (checkBoxVoronoi.Checked && vd != null)) {
					tr.Vertices.ForEach(v => drawVertex(g, centerPen, v.x, v.y));
				}
				
				//circles
				Point2D center = GeomUtils.TriangleCircumcenter(tr.Vertices[0], tr.Vertices[1], tr.Vertices[2]);

				if (checkBoxShowCenters.Checked) {
					drawVertex(g, circlePen, center.x, center.y);
				}
				if (checkBoxCircles.Checked) {
					drawCircle(g, circlePen, center.x, center.y, GeomUtils.DistEuclid(center, tr.Vertices[0]));
				}
			}

			//voronoi
			if (checkBoxVoronoi.Checked && vd != null) {
				vd.Edges.ForEach(edge => drawEdge(g, edgeVoronoiPen, edge.a, edge.b));
			}

		}



		private void drawEdge(Graphics g, Pen pen, Point2D a, Point2D b) {
			TriangleSet.Edge edge = new TriangleSet.Edge(a, b);
			if (!paintedEdges.Contains(edge)) {
				g.DrawLine(pen, a.x, a.y, b.x, b.y);
				paintedEdges.Add(edge);
			}
		}

		private void drawVertex(Graphics g, Pen pen, float x, float y, int size = 7) {
			Point2D point = new Point2D(x, y);
			if (!paintedVertices.Contains(point)) {
				g.DrawRectangle(pen, x - size / 2, y - size / 2, size, size);
				if (checkBoxVoronoi.Checked && paintedVoronoiCellVertex != null
					&& paintedVoronoiCellVertex.Value.Equals(point)) {
					drawCircle(g, pen, x, y, 9);
				}
				paintedVertices.Add(point);
			}
		}

		private void drawCircle(Graphics g, Pen pen, float x, float y, float r) {
			g.DrawEllipse(pen, x - r, y - r, r * 2, r * 2);
		}

		



		private void panel1_MouseClick(object sender, MouseEventArgs e) {

			if (e.Button == System.Windows.Forms.MouseButtons.Left) {
				//DateTime start = DateTime.Now;
				dt.AddPoint(new Point2D(
					checkBoxGrid.Checked ? snapToGrid(e.X) : e.X,
					checkBoxGrid.Checked ? snapToGrid(e.Y) : e.Y));
				//Console.WriteLine("Delaunay: " + (DateTime.Now - start).TotalMilliseconds);
				//start = DateTime.Now;
				vd = new VoronoiDiagram(dt);
				//Console.WriteLine("Voronoi: " + (DateTime.Now - start).TotalMilliseconds);
				paintedVoronoiCellVertex = null;
				panel1.Invalidate();

			} else if (e.Button == System.Windows.Forms.MouseButtons.Right && checkBoxVoronoi.Checked) {
				Point2D point = new Point2D(e.X, e.Y);
				Point2D? closest = null;
				float minDist = float.MaxValue;
				foreach (Point2D vertex in vd.CellPolygons.Keys) {
					float dist = GeomUtils.DistEuclidSq(vertex, point);
					if (dist < minDist) {
						minDist = dist;
						closest = vertex;
					}
				}
				paintedVoronoiCellVertex = closest;
				panel1.Invalidate();
			}
		}



		private int snapToGrid(int coord) {
			return coord % GRID_SIZE >= GRID_SIZE / 2 ?
				(coord + GRID_SIZE - coord % GRID_SIZE)
				: (coord - coord % GRID_SIZE);
		}





		private void checkBoxVoronoi_CheckedChanged(object sender, EventArgs e) {
			panel1.Invalidate();
		}

		private void checkBoxDelaunay_CheckedChanged(object sender, EventArgs e) {
			panel1.Invalidate();
		}

		private void checkBoxShowCenters_CheckedChanged(object sender, EventArgs e) {
			panel1.Invalidate();
		}

		private void checkBoxCircles_CheckedChanged(object sender, EventArgs e) {
			panel1.Invalidate();
		}

		private void checkBoxGrid_CheckedChanged(object sender, EventArgs e) {
			panel1.Invalidate();
		}



		private void buttonAddPoints_Click(object sender, EventArgs e) {
			Enumerable.Range(0, (int)numericUpDownAddPoints.Value).ForEach(_ =>
				dt.AddPoint(new Point2D(rand.Next(25, panel1.Width - 25), rand.Next(25, panel1.Height - 25))));
			vd = new VoronoiDiagram(dt);
			paintedVoronoiCellVertex = null;
			panel1.Invalidate();
		}



		private void buttonClear_Click(object sender, EventArgs e) {
			dt = new DelaunayTriangulation();
			paintedVoronoiCellVertex = null;
			vd = null;
			panel1.Invalidate();
		}

		

	}
}
