﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DelaunayTriangulation {
	
	/// <summary>
	/// Stores triangulation of points and allows basic operations on it.
	/// Data structure is internally represented as set of triangles, each storing its vertices and neighbours
	/// in triangulation. List of edges is stored separately too, storing its endpoints and adjacent triangles
	/// on both of its sides. This is used to easily find neighbours for new triangles.
	/// </summary>
	public class TriangleSet {


		
		private static int idGlobal = 1;


		public class Triangle {

			public Point2D[] Vertices;
			public Triangle[] Neighbours;
			public int id { get; private set; } //used to debug only

			public Triangle(Point2D a, Point2D b, Point2D c) {
				Vertices = new Point2D[] { a, b, c };
				Neighbours = new Triangle[3];
				id = idGlobal++;
			}

			/// <summary>
			/// Applies specified action on each edge of the triangle.
			/// Action obtains index of the current edge and its two vertices.
			/// </summary>
			public void ForEachEdge(Action<int, Point2D, Point2D> action) {
				for (int i = 0; i < 3; ++i) {
					action(i, Vertices[i], Vertices[i + 1 > 2 ? 0 : i + 1]);
				}
			}

			public Point2D OppositeVertex(Triangle neighbour) {
				return Vertices.Where(v => !v.In(neighbour.Vertices)).FirstOrDefault();
			}

			public Point2D OppositeVertex(Edge edge) {
				if (Vertices[0].In(edge.a, edge.b) && Vertices[1].In(edge.a, edge.b)) {
					return Vertices[2];
				}
				if (Vertices[1].In(edge.a, edge.b) && Vertices[2].In(edge.a, edge.b)) {
					return Vertices[0];
				}
				return Vertices[1];
			}

			public IEnumerable<Point2D> AdjacentVertices(Triangle neighbour) {
				return Vertices.Where(v => v.In(neighbour.Vertices));
			}

			public Edge CommonEdge(Triangle neighbour) {
				Point2D[] pts = Vertices.Where(v => v.In(neighbour.Vertices)).ToArray();
				return pts.Length == 2 ? new Edge(pts[0], pts[1]) : null;
			}

			public override string ToString() {
				return id + ". " + Vertices[0].ToString() + ", " + Vertices[1].ToString() + ", " + Vertices[2].ToString() + ": "
					+ (Neighbours[0] == null ? "-" : Neighbours[0].id.ToString()) + ", "
					+ (Neighbours[1] == null ? "-" : Neighbours[1].id.ToString()) + ", "
					+ (Neighbours[2] == null ? "-" : Neighbours[2].id.ToString());
			}
		}


		




		/// <summary>
		/// Represents edge (line segment) consisting of two endpoints.
		/// Endpoints may be swapped, so that edge AB is the same as BA (used fo comparision).
		/// </summary>
		public class Edge {
			
			public Point2D a, b;
			
			public Edge(Point2D a, Point2D b) {
				var pts = new Point2D[] { a, b }.OrderBy(p => p.x).ThenBy(p => p.y);
				this.a = pts.First();
				this.b = pts.Last();
			}

			public override string ToString() {
				return a.ToString() + ", " + b.ToString();
			}

			public override bool Equals(object obj) {
				Edge e = obj as Edge;
				return e != null && e.a.Equals(a) && e.b.Equals(b);
			}

			public override int GetHashCode() {
				return a.GetHashCode() * (1 << 15) + b.GetHashCode();
			}
		}




		public List<Triangle> Triangles { get; private set; }
		
		/// <summary>
		/// Stores all edges and their adjacent triangles.
		/// </summary>
		public Dictionary<Edge, Tuple<Triangle, Triangle>> EdgeTriangle { get; private set; }

		public HashSet<Point2D> Points { get; private set; }



		public TriangleSet() {
			Triangles = new List<Triangle>();
			EdgeTriangle = new Dictionary<Edge, Tuple<Triangle, Triangle>>();
			Points = new HashSet<Point2D>();
		}

		



		/// <summary>
		/// Adds triangle with speicified vertices. Vertices can be already present in triangle set.
		/// Returns newly created triangle. Works in O(1).
		/// </summary>
		public Triangle AddTriangle(Point2D v1, Point2D v2, Point2D v3) {
			Points.Add(v1); Points.Add(v2); Points.Add(v3);
			Triangle tr = new Triangle(v1, v2, v3);
			tr.ForEachEdge((i, tv1, tv2) => updateEdgeAdded(tr, i, tv1, tv2));
			Triangles.Add(tr);
			return tr;
		}

		private Triangle updateEdgeAdded(Triangle tr, int edgeIdx, Point2D a, Point2D b) {
			Edge edge = new Edge(a, b);
			//add new edge with only one neighbour
			if (!EdgeTriangle.ContainsKey(edge)) {
				EdgeTriangle[edge] = Tuple.Create<Triangle, Triangle>(tr, null);
				return null;
			//update edge and its adjacent triangle
			} else {
				Tuple<Triangle, Triangle> neighbours = EdgeTriangle[edge];
				EdgeTriangle[edge] = Tuple.Create(neighbours.Item1, tr);
				tr.Neighbours[edgeIdx] = neighbours.Item1;
				//update neighbour record
				neighbours.Item1.ForEachEdge((i, v1, v2) => {
					if (v1.In(a, b) && v2.In(a, b)) {
						neighbours.Item1.Neighbours[i] = tr;
					}
				});
				return neighbours.Item1;
			}
		}





		/// <summary>
		/// Removes triangle. Doesn't remove its vertices, but removes edges that don't
		/// have any adjacent triangles after removing specified triangle.
		/// Works in O(1);
		/// </summary>
		public void RemoveTriangle(Triangle tr) {
			Triangles.Remove(tr);
			tr.ForEachEdge((i, v1, v2) => updateEdgeRemoved(tr, i, v1, v2));
		}

		private void updateEdgeRemoved(Triangle tr, int edgeIdx, Point2D a, Point2D b) {
			Edge edge = new Edge(a, b);
			Triangle neighbour = tr.Neighbours[edgeIdx];
			//no neighbour, remove edge completely
			if (neighbour == null) {
				EdgeTriangle.Remove(edge);
			//update edge
			} else {
				//remove triangle from neighbour
				neighbour.ForEachEdge((i, v1, v2) => {
					if (neighbour.Neighbours[i] == tr) {
						neighbour.Neighbours[i] = null;
					}
				});
				EdgeTriangle[edge] = Tuple.Create<Triangle, Triangle>(neighbour, null);
			}
		}





		/// <summary>
		/// Adds new vertex inside triangle, replacing the current one by three new smaller ones.
		/// Returns created triangles. Works in O(1).
		/// </summary>
		public IEnumerable<Triangle> AddVertexInsideTriangle(Point2D point, Triangle tr) {
			RemoveTriangle(tr);
			yield return AddTriangle(tr.Vertices[0], tr.Vertices[1], point);
			yield return AddTriangle(point, tr.Vertices[1], tr.Vertices[2]);
			yield return AddTriangle(tr.Vertices[0], point, tr.Vertices[2]);
		}





		/// <summary>
		/// Returns triangle that contains given point.
		/// If point isn't contained by any triangle, returns <c>null</c>.
		/// Runs in O(triangles) = O(vertices).
		/// </summary>
		public Triangle GetContainingTriangle(Point2D point) {
			//check boundaries first to avoid unnecessary point in triangle calculations
			return Triangles.Where(t =>
				t.Vertices.Min(v => v.x) <= point.x &&
				t.Vertices.Max(v => v.x) >= point.x &&
				t.Vertices.Min(v => v.y) <= point.y &&
				t.Vertices.Max(v => v.y) >= point.y &&
				GeomUtils.PointInTriangle(point, t.Vertices[0], t.Vertices[1], t.Vertices[2]))
				.FirstOrDefault();
		}





		/// <summary>
		/// Returns all visible edges and their corresponding triangles visible from a vertex outside
		/// of covex hull of current point set. Corresponding triangles will be always on boundary,
		/// so found edges have only this one adjacent triangle.
		/// Runs in O(triangles^2) = O(vertices^2) with some ugly constants.
		/// </summary>
		public IEnumerable<Tuple<Triangle, Point2D, Point2D>> GetVisibleEdges(Point2D point) {
			var boundaryTriangles = Triangles.Where(t => t.Neighbours.Any(n => n == null));
			var result = new List<Tuple<Triangle, Point2D, Point2D>>();

			//ALGORITHM:
			//1. check every edge without neighbour of every boundary triangle
			//2. try to connect point with edge endpoints and check whether created line segments intersect with
			//   any other triangle. by this we will find whether potentionally created triangle edges collides with others.
			//   notice that we only need to check intesection with boundary triangles.
			//3. we also need to avoid situation when newly created triangle will contain other smaller triangle within
			//   itself. in this case, checking only new triangle edge intersections is not enough.
			//   we thus check for intersection between new vertex and existing visible edge midpoint.
			//   if newly created triangle contains smaller one, this line segment will intersect with it.
			foreach (Triangle tr in boundaryTriangles) {
				for (int i = 0; i < 3; ++i) {
					if (tr.Neighbours[i] == null) {
						
						Point2D v1 = tr.Vertices[i];
						Point2D v2 = tr.Vertices[i == 2 ? 0 : (i + 1)];
						Point2D v3 = tr.Vertices[i == 0 ? 2 : (i == 1 ? 0 : 1)];
						Point2D mid = GeomUtils.LineSegmentMidpoint(v1, v2);
						
						if (!GeomUtils.LineSegmentsIntersect(point, mid, v1, v3) &&
							!GeomUtils.LineSegmentsIntersect(point, mid, v2, v3) &&
							boundaryTriangles.All(tr2 =>
								!segmentIntersectsTriangle(point, v1, tr2) &&
								!segmentIntersectsTriangle(point, v2, tr2))) {
									result.Add(Tuple.Create(tr, v1, v2));
						}
					}
				}
			}

			return result;
		}


		private bool segmentIntersectsTriangle(Point2D a, Point2D b, Triangle tr) {
			bool found = false;
			tr.ForEachEdge((_, v1, v2) => {
				found |= GeomUtils.LineSegmentsIntersect(a, b, v1, v2, true);
			});
			return found;
		}

	}
}
