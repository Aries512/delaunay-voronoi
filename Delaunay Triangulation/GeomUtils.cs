﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace DelaunayTriangulation {
	public static class GeomUtils {



		public const float EPS = 1e-6f;



		public static Point2D LineSegmentMidpoint(Point2D a, Point2D b) {
			return new Point2D((a.x + b.x) / 2, (a.y + b.y) / 2);
		}



		/// <summary>
		/// Finds whether a point is on line segment ab.
		/// </summary>
		public static bool PointOnLineSegment(Point2D point, Point2D a, Point2D b, bool includeEndpoints = true) {
			bool inYRange = includeEndpoints ?
						(point.y >= Math.Min(a.y, b.y) && point.y <= Math.Max(a.y, b.y))
						: (point.y > Math.Min(a.y, b.y) && point.y < Math.Max(a.y, b.y));
			bool inXRange = includeEndpoints ?
						(point.x >= Math.Min(a.x, b.x) && point.x <= Math.Max(a.x, b.x))
						: (point.x > Math.Min(a.x, b.x) && point.x < Math.Max(a.x, b.x));
			if (a.x == b.x) {
				return point.x == a.x ? inYRange : false;
			}
			if (a.y == b.y) {
				return point.y == a.y ? inXRange : false;
			}
			return inXRange && inYRange &&
				Math.Abs((point.x - a.x) / (b.x - a.x) - (point.y - a.y) / (b.y - a.y)) < EPS;
		}


		
		public static bool LineSegmentsIntersect(Point2D a1, Point2D a2, Point2D b1, Point2D b2, bool canOverlapInEndpoints = false) {
			
			//check endpoints overlap
			if (!canOverlapInEndpoints && (a1.Equals(b1) || a1.Equals(b2) || a2.Equals(b1) || a2.Equals(b2))) {
				return true;
			}
			
			//check point on line segment
			if (PointOnLineSegment(a1, b1, b2, false) || PointOnLineSegment(a2, b1, b2, false) ||
				PointOnLineSegment(b1, a1, a2, false) || PointOnLineSegment(b2, a1, a2, false)) {
				return true;
			}
			
			//check actual segment intersection
			float tmax = (b2.y - b1.y) * (a2.x - a1.x) - (b2.x - b1.x) * (a2.y - a1.y);
			float t1 = (b2.x - b1.x) * (a1.y - b1.y) - (b2.y - b1.y) * (a1.x - b1.x);
			float t2 = (a2.x - a1.x) * (a1.y - b1.y) - (a2.y - a1.y) * (a1.x - b1.x);
			
			if (((t1 > 0) != (tmax > 0)) || ((t2 > 0) != (tmax > 0))) return false;

			tmax = Math.Abs(tmax); t1 = Math.Abs(t1); t2 = Math.Abs(t2);
			return 0 < t1 && t1 < tmax && 0 < t2 && t2 < tmax;
		}



		public static bool PointsAligned(params Point2D[] points) {
			points.ThrowIfNull();
			if (points.Length < 3 || points.All(p => p.y == points[0].y) || points.All(p => p.x == points[0].x)) {
				return true;
			}
			points = points.Select(p => new Point2D(p.x - points[0].x, p.y - points[0].y))
				.OrderBy(p => p.x).ToArray();
			return points.All(p => Math.Abs(p.y / points.Last().y - p.x / points.Last().x) < EPS);

		}



		public static float DistEuclid(Point2D a, Point2D b) {
			return (float)Math.Sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
		}

		public static float DistEuclidSq(Point2D a, Point2D b) {
			return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y);
		}



		//http://en.wikipedia.org/wiki/Circumscribed_circle#Cartesian_coordinates
		public static Point2D TriangleCircumcenter(Point2D a, Point2D b, Point2D c) {
			Point2D aTmp = a;
			b.x -= a.x;
			b.y -= a.y;
			c.x -= a.x;
			c.y -= a.y;
			a.x = a.y = 0;
			float d = 2 * (b.x * c.y - b.y * c.x);
			float x = (c.y * (b.x * b.x + b.y * b.y) - b.y * (c.x * c.x + c.y * c.y)) / d;
			float y = (b.x * (c.x * c.x + c.y * c.y) - c.x * (b.x * b.x + b.y * b.y)) / d;
			return new Point2D(x + aTmp.x, y + aTmp.y);
		}



		public static bool PointInTriangleCircumcircle(Point2D p, Point2D ta, Point2D tb, Point2D tc) {
			Point2D center = TriangleCircumcenter(ta, tb, tc);
			return DistEuclidSq(p, center) < DistEuclidSq(ta, center);
		}



		//http://stackoverflow.com/questions/2049582/how-to-determine-a-point-in-a-triangle
		/// <summary>
		/// Finds whether a point is in triangle abc or on its edges.
		/// </summary>
		public static bool PointInTriangle(Point2D point, Point2D a, Point2D b, Point2D c) {
			bool b1 = sign(point, a, b) < 0.0f;
			bool b2 = sign(point, b, c) < 0.0f;
			bool b3 = sign(point, c, a) < 0.0f;
			return ((b1 == b2) && (b2 == b3));
		}

		private static float sign(Point2D p1, Point2D p2, Point2D p3) {
			return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
		}
		


	}
}
